import nltk
# Leggere file treebank
in_file = open("C:\\Users\\Anna\\Documents\\NetBeansProjects\\pcky\docs\\new-treebank-NoNP.penn","r")
#text = in_file.read()

from nltk import Tree
from nltk.grammar import ProbabilisticProduction, Nonterminal

#impostare codifica utf-8, di default ha unicode
import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')


# extract productions from three trees and induce the PCFG
print("Induce PCFG grammar from treebank data:")

productions = [] 
for x in range(0, 985):
       linea = in_file.readline()
	   lin = linea.decode('utf-8')
       tree = Tree.fromstring(lin)   #provare subtree?
	   tree.collapse_unary(true, true, '+')     #tree.collapse_unary()
       tree.chomsky_normal_form()
	   prod = tree.productions()
       productions.extend(prod)
  

in_file.close()

from nltk.grammar import induce_pcfg

S = Nonterminal('S')
grammar = induce_pcfg(S, productions)
sys.stdout = open('grammar345.txt', 'w')
print(grammar)